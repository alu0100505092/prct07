Nodo = Struct.new(:value, :next)

class Lista 
    def initialize(nodo)
        @inicio = Nodo.new(nil)
    end    
    
    def push(nodo)
        if @inicio != nil
            nodo.next = @inicio
            @inicio = nodo
        end        
    end    
    
    def pop(nodo)
        @inicio = @inicio.next
    end
    
    def ext 
        aux = @inicio
        @inicio = @inicio.next
        aux.value
    end    
    
    def inicio
        @inicio
    end    
end