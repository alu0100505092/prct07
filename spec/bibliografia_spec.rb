require 'spec_helper'


describe Bibliografia do
    before :all do 
        @book  = Libro.new({:autor => ["Dave Thomas", "Andy Hunt", "Chad Fowler"], :titulo => "Programming Ruby 1.9 & 2.0: The Pragmatic Programmers Guide", :serie => "The Facets of Ruby", :editorial => "Pragmatic Bookshelf", :edicion => "4 edition", :f_pub => "July 7, 2013", :isbn => ["ISBN-13: 978-1937785499", "ISBN-10: 1937785491"]})
        @book1 = Libro.new({:autor => ["Scott Chacon"], :titulo => "Pro Git 2009th Edition", :serie => "Pro", :editorial => "Apress", :edicion => "2009 edition", :f_pub => "August 27, 2009", :isbn => ["ISBN-13: 978-1430218333", "ISBN-10: 1430218339"]})
        @book2 = Libro.new({:autor => ["David Flanagan", "Yukihiro Matsumoto"], :titulo => "The Ruby Programming Language", :serie => "Serie", :editorial => "O’Reilly Media", :edicion => " 1 edition", :f_pub => "February 4, 2008", :isbn => ["ISBN-10: 0596516177", "ISBN-13: 978-0596516178"]})
        @book3 = Libro.new({:autor => ["David Chelimsky", "Dave Astels", " Bryan Helmkamp", "Dan North", "Zach Dennis", "Aslak Hellesoy"], :titulo => "The RSpec Book: Behaviour Driven Development with RSpec, Cucumber, and Friends", :serie => "The Facets of Ruby", :editorial => "Pragmatic Bookshelf", :edicion => "1 edition", :f_pub => "December 25, 2010", :isbn => ["ISBN-10: 1934356379", "ISBN-13: 978-1934356371",]})
        @book4 = Libro.new({:autor => ["Richard E. Silverman"], :titulo => "Git Pocket Guide", :serie => "Serie", :editorial => "O’Reilly Media", :edicion => "1 edition", :f_pub => "August 2, 2013", :isbn => ["ISBN-10: 1449325866", "ISBN-13: 978-1449325862"]})    
        
        @nodo  = Nodo.new(@book)
        @nodo1 = Nodo.new(@book1)
        @nodo2 = Nodo.new(@book2)
        @nodo3 = Nodo.new(@book3)
        @nodo4 = Nodo.new(@book4)
        
        @lista = Lista.new(0)
  
        
        
    end  

    
    context "Nodo" do
        it "Debe existir un Nodo de la lista con sus datos y su siguiente" do
            expect(@nodo.value).to eq(@book)
            expect(@nodo.next).to eq(nil)
        end
    end    
    
    context "Lista" do
        it "Se extrae el primer elemento de la lista" do
            @lista.push(@nodo4)
            @lista.push(@nodo3)
            @lista.push(@nodo2)
            @lista.push(@nodo1)
            @lista.push(@nodo)
            @lista.ext
            expect(@lista.inicio).to eq(@nodo1)
        end
        
        it "Se puede insertar un elemento" do
            @lista.push(@nodo4)
            expect(@lista.inicio).to eq(@nodo4)
        end
        
        it "Se pueden insertar varios elementos" do
            @lista.push(@nodo4)
            @lista.push(@nodo3)
            @lista.push(@nodo2)
            @lista.push(@nodo1)
            @lista.push(@nodo)
            expect(@lista.inicio).to eq(@nodo)
            @lista.ext
            expect(@lista.inicio).to eq(@nodo1)
            @lista.ext
            expect(@lista.inicio).to eq(@nodo2)
            @lista.ext
            expect(@lista.inicio).to eq(@nodo3)
            @lista.ext
            expect(@lista.inicio).to eq(@nodo4)
        end
        
        it "Debe existir una lista con su cabeza" do
            @lista.push(@nodo)
            expect(@lista.inicio).to eq(@nodo)
        end
    end
end

